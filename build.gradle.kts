import com.bmuschko.gradle.docker.tasks.image.DockerBuildImage
import com.bmuschko.gradle.docker.tasks.image.DockerPushImage
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.2.0.M5"
    id("io.spring.dependency-management") version "1.0.8.RELEASE"
    id("com.bmuschko.docker-spring-boot-application") version "5.0.0"
    id("com.patdouble.awsecr") version "0.5.1"
    id("org.jetbrains.kotlin.plugin.jpa") version "1.3.41"
    kotlin("jvm") version "1.3.41"
    kotlin("plugin.spring") version "1.3.41"
    groovy
}

group = "com.mercadolibre"
version = "0.0.1"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
    mavenCentral()
    maven { url = uri("https://repo.spring.io/milestone") }
    maven { url = uri("https://repo.spring.io/snapshot") }
    maven { url = uri("http://oss.jfrog.org/artifactory/oss-snapshot-local/") }
}

dependencies {
    implementation("io.springfox:springfox-swagger2:3.0.0-SNAPSHOT")
    implementation("io.springfox:springfox-swagger-ui:3.0.0-SNAPSHOT")
    implementation("io.springfox:springfox-spring-webflux:3.0.0-SNAPSHOT")
    implementation("org.springframework.plugin:spring-plugin-core:1.2.0.RELEASE")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.7.3")

    implementation("org.springframework.boot:spring-boot-starter-data-cassandra-reactive")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("io.projectreactor.kafka:reactor-kafka:1.1.0.RELEASE")
    implementation("com.impossibl.pgjdbc-ng:pgjdbc-ng:0.8.2")
    implementation("org.postgresql:postgresql:42.2.6")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("io.projectreactor:reactor-test")
    testImplementation("org.codehaus.groovy:groovy-all:2.5.4")
    testCompile("org.spockframework:spock-core:1.3-groovy-2.5")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

docker {
    registryCredentials {
        url.set("https://432197956285.dkr.ecr.us-west-2.amazonaws.com")
        username.set("AWS")
        password.set("eyJwYXlsb2FkIjoiRnZqU3ozTVNvQjRSYzhOREU4V3g1UmVjdjZUdWFGNkhNSzdQSmdFUlpqbkZIZisvSzVaQnpTcXI3TGJJQzd2YmlFMkxKVjByN3gzOXM0dVdCbzk2MFgrc3hydzZnelRPSnZWc0djK0ZiU2NwMjR5UTlBOGxlOWZGbXVRRTlzc3hTYkF6L2d2T2RlOGtKbG5rTVd2ZFpUcVIzeXVraVcvVmxuVmR4TVN1OXMvWUNyOThMcGppNHlOOTRIYkVMaEdqTWgrT1ZIcnh2L0tjTmZUSzFQNmhQQzA2ZFlTb0k2RzlsK296YUIwTmVHU25EM1dzSnJrdHZhTHU0bElESlArK2JGRW9aaGFDSzVyTHhqVWpqVzQxM1N3U0FFUnVHYzl1R1k0ODNaUUdRWGNaRGRzRWxhYlFrYVBmOUJQRFhiL0xDVlViQTR3dWQwNkRDN0NFemhMdmx5bkNDUVpLcjI4dnFQYlFSWEVqNmRXMWlaUmU4ZHE1SEkyWHZSVEZydmExSzdaQTdBWjljcU9wQVAvbFVqVGUyeGpTaGJMQ3pVazVFOGxwQTZDclhab1pqc1NDZks2TUg5eXBoL0QzOVloRWxFUGNPSUs0ZHBxbE8rZDNtTEw3WGZSRFFNSjlwaTk4YUVnQmQ1cCs5c2ZRaGE2MEdCc29NbktZSWF6WnN3a0E0ZG5SQk5uZHpwd3RBZlY2bDlicG9MUDdOTHNkdG0vQmt0WHFkTVNQU3VMVEhKcVdkdzFSYjRqOVkydE01L3NTVUZTRFpsd3RvV3lMOXVzL0VnWHFLY2lmVWRJek5GdnRmTE9JYWRMWFNMb0w2a20yb1p2enBDMHZ6Z1hyV1NlNi94TUJHSlRrSWZGVmsxc1h2YmNvdGZyVEtRVDNjREZxRG5yYUlRTUVnbGRHdzNqVUd5c2F1cFlvNjhuWTJqbWNjYyt5emtkZkNPZW9TR21yRzAva3ZnUk5ka2Y0bU5FaWdKNXZXNExlSHNQb0V0OGtsUU5xNHpNRlVBVFpQTEZYeXhacXNSUXFQbldkb1dTcCt2R2VLSE9QYUlvZFJrVzR3anNvRk1POFAxTGh3aHhTNEpHdXRuVHlNNUwrZllaVUZsWElXQmRmVzI0cW1qRm9uRWhJeDZxRkt4VXZnOFpUWlFTZElvV3pQNlA5Y1k0ZU8rRTdQVXV4VlhoV2RKY3d3NjR3ZDNINFVROW9hbVNodE43MDVuTllWZU0rcXBUbTVOVVQvSWJtSWlybXlYaXFYU1JxQW8vMVZYRTZuaXJDbFVNL2o4ejN5eE10ZTFJMDZNZy9zWmRqTDZTZm9iTlJiZDdpdlpHcFM5UEhzU1pHL0pvcUs4LzBjSXJSMXo5SCtmbk5rYmU3NjA2L1NCWm54NHYyUi9TUExVdzcyOHZWeFJZS0lUN3JtRFJGVXlLb2RqMFBEaFdzdlRXcWlWbEQySTA9IiwiZGF0YWtleSI6IkFRRUJBSGo2bGM0WElKdy83bG4wSGMwMERNZWs2R0V4SENiWTRSSXBUTUNJNThJblV3QUFBSDR3ZkFZSktvWklodmNOQVFjR29HOHdiUUlCQURCb0Jna3Foa2lHOXcwQkJ3RXdIZ1lKWUlaSUFXVURCQUV1TUJFRURCelpVNmVoditYd3lLOHZJQUlCRUlBN0YzZm1GdGR3QjN6Y3J3TDJuMCtYUHN3ZEZGQUl5YWx6OVFwMnJ0cHIvSFVoL1I0YTZtK1lGa2krb2kwR0dPY3hSWWwxS2JtekhmWmZGbGM9IiwidmVyc2lvbiI6IjIiLCJ0eXBlIjoiREFUQV9LRVkiLCJleHBpcmF0aW9uIjoxNTY3NzY5MjgwfQ==")
    }
}

tasks.create<DockerBuildImage>("buildImage") {
    dependsOn("dockerCreateDockerfile")
    tags.add("432197956285.dkr.ecr.us-west-2.amazonaws.com/challenge-meli:latest")
}

tasks.create<DockerPushImage>("pushImage") {
    dependsOn("buildImage")
    imageName.set("432197956285.dkr.ecr.us-west-2.amazonaws.com/challenge-meli:latest")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}
