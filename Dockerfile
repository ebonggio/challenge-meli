FROM openjdk:jre-alpine
LABEL maintainer=enzobonggio
WORKDIR /app
COPY build/docker/libs libs/
COPY build/docker/resources resources/
COPY build/docker/classes classes/
ENTRYPOINT ["java", "-cp", "/app/resources:/app/classes:/app/libs/*", "com.mercadolibre.challenge.ChallengeApplicationKt"]
EXPOSE 8080
