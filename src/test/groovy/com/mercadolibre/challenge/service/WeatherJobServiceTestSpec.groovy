package com.mercadolibre.challenge.service

import com.mercadolibre.challenge.exception.NotFound
import com.mercadolibre.challenge.model.WeatherJob
import com.mercadolibre.challenge.repository.postgres.WeatherJobRepository
import spock.lang.Specification

class WeatherJobServiceTestSpec extends Specification {

    def "test ok find last job"() {
        given: "Job Mock and Service"
        def expected = GroovyMock(WeatherJob)
        def weatherJobRepository = Mock(WeatherJobRepository) {
            findTopByAndEndTimeIsNotNullOrderByCycleDesc() >> expected
        }
        def service = new WeatherJobService(
                Mock(PlanetService),
                Mock(WeatherService),
                weatherJobRepository)

        when: "Find Last Job"
        def result = service.findLastJob()
        then: "Check that is the same as expected"
        result == expected
    }

    def "test errors find last job"() {
        given: "Job Mock and Service"
        def weatherJobRepository = Mock(WeatherJobRepository) {
            findTopByAndEndTimeIsNotNullOrderByCycleDesc() >> null
        }
        def service = new WeatherJobService(
                Mock(PlanetService),
                Mock(WeatherService),
                weatherJobRepository)

        when: "Find Last Job"
        def _ = service.findLastJob()
        then: "Check that is the same as expected"
        thrown NotFound
    }
}
