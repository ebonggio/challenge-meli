package com.mercadolibre.challenge.utils

import com.mercadolibre.challenge.model.CardinalPosition
import spock.lang.Specification

class MathUtilsTestSpec extends Specification {

    def "test collinear"(x1, y1, x2, y2, x3, y3, result) {
        given: "3 points as a List"
        def list = [new CardinalPosition(x1, y1), new CardinalPosition(x2, y2), new CardinalPosition(x3, y3)]

        when: "Test collinear with $list"
        def collinearResult = use(MathUtilsKt) {
            list.checkCollinear()
        }

        then: "Expect result $result"
        def isCollinear
        switch (collinearResult) {
            case 0:
                isCollinear = Collinear.IS
                break
            default:
                isCollinear = Collinear.IS_NOT
                break
        }

        isCollinear == result

        where:
        x1 | y1 | x2 | y2 | x3 | y3  | result
        5  | 8  | 5  | 9  | 5  | 100 | Collinear.IS
        1  | 10  | 3  | 5  | 5  | 15   | Collinear.IS_NOT
    }

    enum Collinear {
        IS,
        IS_NOT
    }


}
