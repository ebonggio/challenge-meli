package com.mercadolibre.challenge.model

import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "weather_jobs", schema = "config")
data class WeatherJob(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        val id: Long? = null,
        val initTime: Timestamp,
        val endTime: Timestamp? = null,
        val cycle: Int? = null)