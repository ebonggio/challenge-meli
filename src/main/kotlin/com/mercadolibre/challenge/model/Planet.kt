package com.mercadolibre.challenge.model

import com.mercadolibre.challenge.utils.DEGREE_CYCLE
import com.mercadolibre.challenge.utils.remAbs
import javax.persistence.*
import kotlin.math.cos
import kotlin.math.sin

@Entity
@Table(name = "planets", schema = "config")
data class Planet(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        val id: Long? = null,
        val name: String,
        @OneToOne(cascade = [CascadeType.ALL])
        @JoinColumn(unique = true)
        val anglePosition: AnglePosition,
        val velocity: Int) {

    @delegate:Transient
    val cardinalPosition by lazy {
        anglePosition.toCardinalPosition()
    }

    fun advance(): Planet {
        return this.copy(
                velocity = velocity,
                anglePosition = anglePosition.copy(
                        distance = anglePosition.distance,
                        degrees = (anglePosition.degrees + velocity).remAbs(DEGREE_CYCLE)))
    }

}

@Entity
@Table(name = "positions", schema = "config")
data class AnglePosition(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        val id: Long? = null,
        val distance: Int,
        val degrees: Int) {

    @delegate:Transient
    private val angle: Double by lazy { Math.toRadians(degrees.toDouble()) }

    fun toCardinalPosition() = CardinalPosition(cos(angle).times(distance), sin(angle).times(distance))
}

data class CardinalPosition(
        val x: Double,
        val y: Double)