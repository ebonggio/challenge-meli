package com.mercadolibre.challenge.model

import org.springframework.data.cassandra.core.cql.PrimaryKeyType
import org.springframework.data.cassandra.core.mapping.Indexed
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn
import org.springframework.data.cassandra.core.mapping.Table
import java.util.*
import kotlin.math.absoluteValue

@Table
data class Weather(
        @PrimaryKeyColumn(
                ordinal = 2,
                type = PrimaryKeyType.PARTITIONED)
        val id: UUID = UUID.randomUUID(),
        @Indexed
        val cyclePosition: Int,
        @Indexed
        val weatherType: WeatherType,
        val perimeter: Double?)

data class WeatherResult(
        val weatherType: WeatherType? = null,
        val info: WeatherInfo,
        val needMoreInfo: Boolean = false) {

    fun mapToWeather(idx: Int): Weather {
        return when (this.weatherType) {
            null -> {
                val nextCollinear = this.info.nextWeather
                        ?.info
                        ?.collinear ?: throw RuntimeException()
                val itCollineal = this.info.collinear ?: throw RuntimeException()
                Weather(
                        weatherType = if ((itCollineal * nextCollinear) < 0) WeatherType.OPTIMAL else WeatherType.NORMAL,
                        cyclePosition = idx,
                        perimeter = this.info.perimeter)
            }
            else ->
                Weather(
                        weatherType = this.weatherType,
                        cyclePosition = idx,
                        perimeter = this.info.perimeter)
        }
    }
}

data class WeatherInfo(
        val collinear: Double? = null,
        val nextWeather: WeatherResult? = null,
        val perimeter: Double? = null)

enum class WeatherType(val pretty: String) {
    DROUGHT("drought"),
    RAIN("rain"),
    OPTIMAL("optimal"),
    NORMAL("normal")
}