package com.mercadolibre.challenge.model

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.LocalDate

data class Forecast(
        val droughtPeriods: List<Period> = emptyList(),
        val rainPeriods: List<RainPeriod> = emptyList(),
        val optimalPeriods: List<Period> = emptyList(),
        val from: LocalDate,
        val to: LocalDate)

fun concatForecast(from: LocalDate, to: LocalDate): (Forecast, Forecast) -> Forecast = { f1, f2 ->
    Forecast(
            from = from,
            to = to,
            droughtPeriods = f1.droughtPeriods + f2.droughtPeriods,
            optimalPeriods = f1.optimalPeriods + f2.optimalPeriods,
            rainPeriods = f1.rainPeriods + f2.rainPeriods)
}


abstract class Period(
        open val from: LocalDate,
        open val to: LocalDate) {
    abstract fun copyAsForecast(): Forecast
}

data class DroughtPeriod(
        override val from: LocalDate,
        override val to: LocalDate) : Period(from, to) {
    override fun copyAsForecast(): Forecast {
        return Forecast(from = from, to = to, droughtPeriods = listOf(this))
    }
}

data class OptimalPeriod(
        override val from: LocalDate,
        override val to: LocalDate) : Period(from, to) {
    override fun copyAsForecast(): Forecast {
        return Forecast(from = from, to = to, optimalPeriods = listOf(this))
    }
}

data class RainPeriod(
        override val from: LocalDate,
        override val to: LocalDate,
        val rainestDay: LocalDate,
        @JsonIgnore
        val rainestDayIndex: Int,
        @JsonIgnore
        val rainestPerimeter: Double) : Period(from, to) {
    override fun copyAsForecast(): Forecast {
        return Forecast(from = from, to = to, rainPeriods = listOf(this))
    }
}
