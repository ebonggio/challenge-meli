package com.mercadolibre.challenge.controller

import com.mercadolibre.challenge.model.Forecast
import com.mercadolibre.challenge.service.ForecastService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@RestController
@RequestMapping("/api/v1/forecast")
class ForecastController(private val forecastService: ForecastService) {

    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    @ApiOperation(value = "Forecast grouping periods", response = Forecast::class, code = 200)
    suspend fun get(
            @ApiParam(name = "from", example = "2019-08-20")
            @RequestParam("from", required = true)
            sFrom: String,
            @ApiParam(name = "to", example = "2020-08-20")
            @RequestParam("to", required = true)
            sTo: String): Forecast {

        val to = LocalDate.parse(sTo, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        val from = LocalDate.parse(sFrom, DateTimeFormatter.ofPattern("yyyy-MM-dd"))

        return forecastService.getForecast(from, to)
    }
}