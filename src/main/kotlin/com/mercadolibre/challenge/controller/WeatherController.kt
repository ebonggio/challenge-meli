package com.mercadolibre.challenge.controller

import com.mercadolibre.challenge.dto.WeatherDTO
import com.mercadolibre.challenge.service.WeatherService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@RestController
@RequestMapping("/api/v1/weathers")
class WeatherController(private val weatherService: WeatherService) {

    @GetMapping
    @ApiOperation(value = "List of weathers", response = WeatherDTO::class, code = 200, responseContainer = "List")
    suspend fun get(
            @ApiParam(name = "day", example = "10")
            @RequestParam("day") day: Int?,
            @ApiParam(name = "from", example = "2019-08-20")
            @RequestParam("from")
            sFrom: String?,
            @ApiParam(name = "to", example = "2020-08-20")
            @RequestParam("to")
            sTo: String?): List<WeatherDTO> {
        val to = sTo?.let { LocalDate.parse(it, DateTimeFormatter.ofPattern("yyyy-MM-dd")) }
                ?: LocalDate.now().plusYears(1)
        val from = sFrom?.let { LocalDate.parse(it, DateTimeFormatter.ofPattern("yyyy-MM-dd")) }
                ?: LocalDate.now()
        return weatherService.weather(day, from, to)
    }
}