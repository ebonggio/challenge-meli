package com.mercadolibre.challenge.controller

import com.mercadolibre.challenge.dto.PlanetDTO
import com.mercadolibre.challenge.service.PlanetService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/v1/planets")
class PlanetController(private val planetService: PlanetService) {

    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    @ApiOperation(value = "List of planets", response = PlanetDTO::class, code = 200, responseContainer = "List")
    suspend fun get(): List<PlanetDTO> = planetService.getAll()

    @PatchMapping("/{id}",
            consumes = [MediaType.APPLICATION_JSON_VALUE],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    @ApiOperation(value = "Planet updated", response = PlanetDTO::class, code = 200)
    suspend fun get(
            @ApiParam(name = "planet_id", example = "1")
            @PathVariable("id") id: Long,
            @Valid @RequestBody dto: PlanetDTO): PlanetDTO? =
            planetService.update(id, dto)
}