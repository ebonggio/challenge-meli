package com.mercadolibre.challenge.repository.postgres

import com.mercadolibre.challenge.model.WeatherJob
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface WeatherJobRepository: JpaRepository<WeatherJob, Long> {

    fun findTopByAndEndTimeIsNotNullOrderByCycleDesc(): WeatherJob?
}
