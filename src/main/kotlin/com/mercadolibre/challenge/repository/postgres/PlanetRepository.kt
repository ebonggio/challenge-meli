package com.mercadolibre.challenge.repository.postgres

import com.mercadolibre.challenge.model.Planet
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PlanetRepository: JpaRepository<Planet, Long>
