package com.mercadolibre.challenge.repository.cassandra

import com.mercadolibre.challenge.model.Weather
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@Repository
interface WeatherRepository : ReactiveCassandraRepository<Weather, UUID> {
    fun findByCyclePosition(pos: Int): Mono<Weather>
    fun findAllByOrderByCyclePosition(): Flux<Weather>
}