package com.mercadolibre.challenge.dto

import com.mercadolibre.challenge.model.WeatherType
import io.swagger.annotations.ApiModel
import java.time.LocalDate
import java.util.*

@ApiModel(description = "Representation of a weather on a particular date")
data class WeatherDTO(
        val id: UUID,
        val date: LocalDate,
        val weather: WeatherType
)