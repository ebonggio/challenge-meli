package com.mercadolibre.challenge.dto

import com.mercadolibre.challenge.model.AnglePosition
import com.mercadolibre.challenge.model.Planet
import io.swagger.annotations.ApiModel
import javax.validation.constraints.Null

@ApiModel(description = "Representation of a planet")
data class PlanetDTO(
        @field:Null
        val id: Long?,
        val name: String?,
        val anglePosition: AnglePositionDTO?,
        val velocity: Int?)

@ApiModel(description = "Representation of planet's position")
data class AnglePositionDTO(
        @field:Null
        val id: Long?,
        val distance: Int?,
        val degrees: Int?)

fun Planet.toPlanetDTO(): PlanetDTO = PlanetDTO(
        this.id,
        this.name,
        AnglePositionDTO(
                this.anglePosition.id,
                this.anglePosition.distance,
                this.anglePosition.degrees),
        this.velocity)

fun Planet.mergeFromDTO(dto: PlanetDTO): Planet {
        return this.copy(
                velocity = dto.velocity ?: this.velocity,
                anglePosition = AnglePosition(
                        this.anglePosition.id,
                        dto.anglePosition?.distance ?: this.anglePosition.distance,
                        dto.anglePosition?.degrees ?: this.anglePosition.degrees))
}