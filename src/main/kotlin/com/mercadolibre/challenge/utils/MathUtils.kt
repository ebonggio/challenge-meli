package com.mercadolibre.challenge.utils

import com.mercadolibre.challenge.model.CardinalPosition
import kotlin.math.pow
import kotlin.math.sqrt

const val DEGREE_CYCLE = 360
const val HALF_DEGREE_CYCLE = 180

fun List<CardinalPosition>.checkCollinear(): Double {
    return when (this.size) {
        2 -> 0.toDouble()
        3 -> Triple(this[0], this[1], this[2]).checkCollinear()
        else -> throw InternalError("Not implemented yet")
    }
}

fun Triple<CardinalPosition, CardinalPosition, CardinalPosition>.checkCollinear(): Double {
    return (this.first.x * (this.second.y - this.third.y) +
            this.second.x * (this.third.y - this.first.y) +
            this.third.x * (this.first.y - this.second.y))
}

fun List<CardinalPosition>.pointInTriangle(): Boolean {
    return when (this.size) {
        2 -> false
        3 -> Triple(this[0], this[1], this[2]).pointInTriangle(CardinalPosition(0.0, 0.0))
        else -> throw InternalError("Not implemented yet")
    }
}

fun Triple<CardinalPosition, CardinalPosition, CardinalPosition>.pointInTriangle(pt: CardinalPosition): Boolean {
    val d1: Double = sign(pt, this.first, this.second)
    val d2: Double = sign(pt, this.second, this.third)
    val d3: Double = sign(pt, this.third, this.first)
    val hasNeg: Boolean
    val hasPos: Boolean
    hasNeg = d1 < 0 || d2 < 0 || d3 < 0
    hasPos = d1 > 0 || d2 > 0 || d3 > 0
    return !(hasNeg && hasPos)
}

fun sign(p1: CardinalPosition, p2: CardinalPosition, p3: CardinalPosition): Double {
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y)
}

suspend fun List<CardinalPosition>.perimeter(): Double {
    return when (this.size) {
        2 -> 0.toDouble()
        3 -> Triple(this[0], this[1], this[2]).perimeter()
        else -> throw InternalError("Not implemented yet")
    }
}

suspend fun Triple<CardinalPosition, CardinalPosition, CardinalPosition>.perimeter(): Double {
    return listOf(Pair(first, second), Pair(second, third), Pair(first, third))
            .pmap { p -> sqrt((p.first.x - p.second.x).pow(2) + (p.first.y - p.second.y).pow(2)) }
            .sum()
}

fun Int.remAbs(mod: Int): Int = this.rem(mod).takeIf { it >= 0 } ?: this.rem(mod) + mod