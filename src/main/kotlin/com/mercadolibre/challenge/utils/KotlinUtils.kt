package com.mercadolibre.challenge.utils

import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import java.lang.RuntimeException
import java.time.LocalDate
import java.util.*
import java.util.stream.LongStream
import kotlin.streams.asSequence


fun <T> Optional<T>.unwrap(): T? = orElse(null)

fun <T, R, T_1> Pair<T, R>.mapFirst(f: (T) -> T_1): Pair<T_1, R> = Pair(f.invoke(first), second)

fun <T, R, R_1> Pair<T, R>.mapSecond(f: (R) -> R_1): Pair<T, R_1> = Pair(first, f.invoke(second))

fun <T, R, T_1, R_1> Pair<T, R>.mapBoth(f1: (T) -> T_1, f2: (R) -> R_1) = Pair(f1.invoke(first), f2.invoke(second))

fun <T> List<T>.asInfiniteSeq(): Sequence<T> = generateSequence { this.asSequence() }.flatMap { it }

fun infiniteDateSeq(startDate: LocalDate): Sequence<LocalDate> =
        LongStream.iterate(0) { i -> i + 1 }
                .mapToObj { i -> startDate.plusDays(i) }
                .asSequence()

suspend fun <A, B> Iterable<A>.pmap(f: suspend (A) -> B): List<B> = coroutineScope {
    map { async { f(it) } }.awaitAll()
}



