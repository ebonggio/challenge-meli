package com.mercadolibre.challenge.utils;

suspend fun calculateCycle(advance: List<Int>, isNow: List<Int> = advance, cicle: Int = 1): Int {
    return when (isNow.sum()) {
        0 -> cicle
        else -> calculateCycle(advance, isNow.zip(advance) { x, y -> x + y }.pmap { it.remAbs(DEGREE_CYCLE) }, cicle + 1)
    }
}