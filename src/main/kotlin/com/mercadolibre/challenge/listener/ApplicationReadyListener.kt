package com.mercadolibre.challenge.listener

import com.mercadolibre.challenge.model.AnglePosition
import com.mercadolibre.challenge.model.Planet
import com.mercadolibre.challenge.service.PlanetService
import com.mercadolibre.challenge.service.WeatherJobService
import kotlinx.coroutines.runBlocking
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

@Component
@Profile("local", "docker", "heroku")
class ReadyListener(val weatherJobService: WeatherJobService,
                    val planetService: PlanetService) : ApplicationListener<ApplicationReadyEvent> {
    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        runBlocking {
            initPlanets()
            weatherJobService.startJob()
        }
    }

    fun initPlanets(): List<Planet> {
        planetService.deleteAll()
        val p1 = Planet(name = "Ferengis", anglePosition = AnglePosition(distance = 500, degrees = 90), velocity = 1)
        val p2 = Planet(name = "Betasoides", anglePosition = AnglePosition(distance = 2000, degrees = 90), velocity = 3)
        val p3 = Planet(name = "Vulcanos", anglePosition = AnglePosition(distance = 1000, degrees = 90), velocity = -5)
        return planetService.saveAll(listOf(p1, p2, p3))
    }
}