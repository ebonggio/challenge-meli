package com.mercadolibre.challenge.listener

import com.impossibl.postgres.api.jdbc.PGConnection
import com.impossibl.postgres.api.jdbc.PGNotificationListener
import com.impossibl.postgres.jdbc.PGDataSource
import com.mercadolibre.challenge.service.WeatherJobService
import kotlinx.coroutines.runBlocking
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import reactor.kafka.receiver.KafkaReceiver
import reactor.kafka.receiver.ReceiverOptions
import javax.annotation.PostConstruct
import kotlin.properties.Delegates

@Component
@Profile("local", "heroku")
class PostgresPlanetListener(
        private val weatherJobService: WeatherJobService,
        private val dataSourceProperties: DataSourceProperties) {

    private val log: Logger = LoggerFactory.getLogger(PostgresPlanetListener::class.java)

    val dataSource by lazy {
        log.trace("Starting listener dataSource with params {}", dataSourceProperties)
        val dataSource = PGDataSource()
        dataSource.host = dataSourceProperties.host
        dataSource.port = dataSourceProperties.port
        dataSource.databaseName = dataSourceProperties.databaseName
        dataSource.user = dataSourceProperties.user
        dataSource.password = dataSourceProperties.password
        dataSource
    }

    fun getNotifications() {
        (dataSource.connection as PGConnection).use {
            try {
                it.addNotificationListener(PlanetNotificationListener(weatherJobService))
                val statement = it.createStatement()
                statement.execute("LISTEN planets")
                statement.close()
                log.trace("Starting listening on planets")
                while (true) {
                }
            } catch (ex: RuntimeException) {
                log.error("There was an error on get postgres listener", ex)
            }
        }

    }

    @PostConstruct
    fun init() {
        Thread(Runnable { getNotifications() }).start()
    }

    @ConfigurationProperties("datasource")
    class DataSourceProperties {
        lateinit var host: String
        var port by Delegates.notNull<Int>()
        lateinit var databaseName: String
        lateinit var user: String
        lateinit var password: String
    }

    inner class PlanetNotificationListener(private val weatherJobService: WeatherJobService) : PGNotificationListener {
        private val log: Logger = LoggerFactory.getLogger(PlanetNotificationListener::class.java)
        override fun notification(processId: Int, channelName: String?, payload: String?) {
            runBlocking {
                log.info("PID {} - Something has change on planets: {}", processId, payload)
                weatherJobService.startJob()
            }
        }
    }
}


@Profile("docker", "prod")
@Component
class KafkaPlanetListener(private val weatherJobService: WeatherJobService, private val dataSourceProperties: KafkaPlanetListener.DataSourceProperties) {
    private val log: Logger = LoggerFactory.getLogger(KafkaPlanetListener::class.java)

    private val consumerProps: Map<String, Any> by lazy {
        mapOf(
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG to StringDeserializer::class.java,
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG to StringDeserializer::class.java,
                ConsumerConfig.CLIENT_ID_CONFIG to "prueba-1",
                ConsumerConfig.GROUP_ID_CONFIG to "prueba",
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG to dataSourceProperties.server//"PLAINTEXT://kafka:9092"
        )
    }

    private val consumerOptions: ReceiverOptions<Any, Any> by lazy {
        ReceiverOptions.create<Any, Any>(consumerProps)
                .subscription(listOf("dbserver1.config.planets", "dbserver1.config.positions"))
                .addAssignListener { log.debug("onPartitionsAssigned {}", it) }
                .addRevokeListener { log.debug("onPartitionsRevoked {}", it) }
    }

    @PostConstruct
    fun init() {
        KafkaReceiver.create(consumerOptions)
                .receive()
                .doOnNext { log.info("Something has change on planets") }
                .map {
                    runBlocking {
                        weatherJobService.startJob()
                    }
                }
                .subscribe()
    }

    @ConfigurationProperties("kafka")
    class DataSourceProperties {
        lateinit var server: String
    }
}