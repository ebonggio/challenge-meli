package com.mercadolibre.challenge.service

import com.mercadolibre.challenge.exception.NotFound
import com.mercadolibre.challenge.model.WeatherJob
import com.mercadolibre.challenge.repository.postgres.WeatherJobRepository
import com.mercadolibre.challenge.utils.calculateCycle
import com.mercadolibre.challenge.utils.pmap
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.sql.Timestamp
import java.time.LocalDateTime

@Service
class WeatherJobService(
        private val planetService: PlanetService,
        private val weatherService: WeatherService,
        private val weatherJobRepository: WeatherJobRepository) {

    private val log: Logger = LoggerFactory.getLogger(WeatherJobService::class.java)

    suspend fun startJob(): WeatherJob {
        return WeatherJob(initTime = Timestamp.valueOf(LocalDateTime.now()))
                .let { weatherJobRepository.save(it) }
                .let { job ->
                    val planets = planetService.getPlanets()
                    val cycle = calculateCycle(planets.pmap { it.velocity })
                    Pair(planets, weatherJobRepository.save(job.copy(cycle = cycle)))
                }
                .let { (planets, job) ->
                    weatherService.deleteAll()
                    val weatherResults = weatherService.weatherCycle(cycle = job.cycle!!, planets = planets)
                    val weathers = weatherResults.mapIndexed { i, ele -> ele.mapToWeather(i) }
                    weatherService.saveAll(weathers)
                    weatherJobRepository.save(job.copy(endTime = Timestamp.valueOf(LocalDateTime.now())))
                }
                .also { log.info("Weather Job finished {}", it) }
    }

    fun findLastJob() = (weatherJobRepository.findTopByAndEndTimeIsNotNullOrderByCycleDesc()
            ?: throw NotFound("No job found"))
            .also { log.info("Found last job {}", it) }
}