package com.mercadolibre.challenge.service;

import com.mercadolibre.challenge.dto.WeatherDTO
import com.mercadolibre.challenge.exception.BadRequest
import com.mercadolibre.challenge.exception.NotFound
import com.mercadolibre.challenge.model.*
import com.mercadolibre.challenge.repository.cassandra.WeatherRepository
import com.mercadolibre.challenge.utils.*
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.temporal.ChronoUnit

@Service
class WeatherService(private val weatherRepository: WeatherRepository,
                     @Lazy private val weatherJobService: WeatherJobService) {

    suspend fun weather(day: Int?, from: LocalDate, to: LocalDate): List<WeatherDTO> {
        return when {
            day != null -> weatherByDay(day)
            else -> weatherByDates(from, to)
                    .map { WeatherDTO(it.first.id, it.second, it.first.weatherType) }
        }
    }

    private suspend fun weatherByDay(day: Int): List<WeatherDTO> {
        val lastJob = weatherJobService.findLastJob()
        val (jobDate, cycle) = Pair(lastJob.initTime, lastJob.cycle)
                .mapFirst { it.toLocalDateTime().toLocalDate() }
                .mapSecond { it ?: throw NotFound("Cycle not found") }
        val weatherDate = LocalDate.now().plusDays(day.toLong())

        val daysFromJobToToday = ChronoUnit.DAYS.between(jobDate, LocalDate.now())
        val realDays = daysFromJobToToday.toInt() + day

        val weather = weatherRepository.findByCyclePosition(realDays.remAbs(cycle))
                .map { WeatherDTO(it.id, weatherDate, it.weatherType) }
                .awaitSingle()
        return listOf(weather)
    }

    suspend fun weatherByDates(from: LocalDate, to: LocalDate): List<Pair<Weather, LocalDate>> {
        val daysBetween = ChronoUnit.DAYS.between(from, to)
        if (daysBetween < 0) throw BadRequest("FROM is greater than TO")

        val (lastJobTime, cycle) = weatherJobService.findLastJob().let {
            Pair(
                    it.endTime?.toLocalDateTime()?.toLocalDate() ?: throw NotFound("No last job time"),
                    it.cycle ?: throw NotFound("No cycle"))
        }
        val startingPoint = ChronoUnit.DAYS.between(lastJobTime, from).rem(cycle).toInt()
        val size = ChronoUnit.DAYS.between(from, to).toInt()
        val sortedWeathers = weatherRepository.findAll()
                .collectSortedList { a1, a2 -> a1.cyclePosition.compareTo(a2.cyclePosition) }
                .awaitSingle()

        return sortedWeathers.asInfiniteSeq()
                .zip(infiniteDateSeq(lastJobTime))
                .drop(startingPoint)
                .take(size)
                .toList()
    }

    suspend fun saveAll(weathers: List<Weather>): List<Weather> = weatherRepository.saveAll(weathers)
            .collectList()
            .awaitSingle()

    suspend fun deleteAll() = weatherRepository.deleteAll().awaitFirstOrNull()

    suspend fun weatherCycle(cycle: Int, previousWeather: WeatherResult? = null, planets: List<Planet>, idx: Int = 0): List<WeatherResult> {
        return when (idx) {
            cycle -> emptyList()
            else -> {
                val advancedPlanets = planets.pmap { it.advance() }
                val nextWeatherResult = advancedPlanets.checkWeather()
                val weatherResult = when (previousWeather?.needMoreInfo) {
                    null -> previousWeather?.let { listOf(it) } ?: emptyList()
                    else -> listOf(previousWeather.copy(info = WeatherInfo(
                            collinear = previousWeather.info.collinear,
                            perimeter = previousWeather.info.perimeter,
                            nextWeather = nextWeatherResult)))
                }
                return weatherResult +
                        weatherCycle(cycle, planets.checkWeather(), advancedPlanets, idx + 1)
            }
        }
    }

    private suspend fun List<Planet>.checkWeather(): WeatherResult {
        val collinear = this.pmap { it.cardinalPosition }.checkCollinear()
        return when {
            this.pmap { it.anglePosition.degrees % HALF_DEGREE_CYCLE }.toSet().size == 1 ->
                WeatherResult(info = WeatherInfo(collinear = collinear), weatherType = WeatherType.DROUGHT)
            this.pmap { it.cardinalPosition }.pointInTriangle() -> WeatherResult(
                    weatherType = WeatherType.RAIN,
                    info = WeatherInfo(
                            collinear = collinear,
                            perimeter = this.pmap { it.cardinalPosition }.perimeter()))
            else -> WeatherResult(info = WeatherInfo(collinear = collinear), needMoreInfo = true)
        }
    }
}