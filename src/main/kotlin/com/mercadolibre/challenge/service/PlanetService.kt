package com.mercadolibre.challenge.service

import com.mercadolibre.challenge.dto.PlanetDTO
import com.mercadolibre.challenge.dto.mergeFromDTO
import com.mercadolibre.challenge.dto.toPlanetDTO
import com.mercadolibre.challenge.exception.NotFound
import com.mercadolibre.challenge.model.Planet
import com.mercadolibre.challenge.repository.postgres.PlanetRepository
import com.mercadolibre.challenge.utils.pmap
import com.mercadolibre.challenge.utils.unwrap
import org.springframework.stereotype.Service

@Service
class PlanetService(val planetRepository: PlanetRepository) {

    suspend fun getPlanets(): List<Planet> = planetRepository.findAll().pmap { it.copy(anglePosition = it.anglePosition.copy()) }

    suspend fun getAll(): List<PlanetDTO> = planetRepository.findAll()
            .pmap { it.toPlanetDTO() }

    fun update(id: Long, dto: PlanetDTO): PlanetDTO? {
        val planet = planetRepository.findById(id).unwrap()
                ?: throw NotFound("Planet not found")

        return planet.copy(anglePosition = planet.anglePosition.copy())
                .mergeFromDTO(dto)
                .let { planetRepository.save(it) }
                .toPlanetDTO()
    }

    fun saveAll(planets: List<Planet>): List<Planet> = planetRepository.saveAll(planets)

    fun deleteAll() = planetRepository.deleteAll()
}