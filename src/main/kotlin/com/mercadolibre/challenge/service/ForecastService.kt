package com.mercadolibre.challenge.service

import com.mercadolibre.challenge.model.*
import com.mercadolibre.challenge.utils.pmap
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class ForecastService(val weatherService: WeatherService) {

    suspend fun getForecast(from: LocalDate, to: LocalDate): Forecast {
        val weathers = weatherService.weatherByDates(from, to)
        val periods = getPeriods(weathers)
        return periods
                .pmap { it.copyAsForecast() }
                .foldRight(Forecast(from = from, to = to), concatForecast(from, to))
    }

    private fun getPeriods(weathers: List<Pair<Weather, LocalDate>>, i: Int = 0): List<Period> {
        return when (i) {
            weathers.size -> emptyList()
            else -> {
                val weatherType = weathers[i].first.weatherType
                val (nextI, period) = when (weatherType) {
                    WeatherType.RAIN -> rainPeriod(weathers, i)
                    WeatherType.DROUGHT -> droughtPeriod(weathers, i, WeatherType.DROUGHT)
                    WeatherType.OPTIMAL -> optimalPeriod(weathers, i, WeatherType.OPTIMAL)
                    WeatherType.NORMAL -> Pair(i + 1, emptyList())
                }
                period + getPeriods(weathers, nextI)
            }
        }
    }

    private fun optimalPeriod(weathers: List<Pair<Weather, LocalDate>>, i: Int,
                              type: WeatherType,
                              r: OptimalPeriod = OptimalPeriod(from = weathers[i].second, to = weathers[i].second)): Pair<Int, List<OptimalPeriod>> {
        return when {
            i == weathers.size || weathers[i].first.weatherType != type -> Pair(i, listOf(r))
            else -> optimalPeriod(weathers, i + 1,
                    type,
                    r.copy(to = weathers[i].second))
        }
    }

    private fun droughtPeriod(weathers: List<Pair<Weather, LocalDate>>, i: Int,
                              type: WeatherType,
                              r: DroughtPeriod = DroughtPeriod(from = weathers[i].second, to = weathers[i].second)): Pair<Int, List<DroughtPeriod>> {
        return when {
            i == weathers.size || weathers[i].first.weatherType != type -> Pair(i, listOf(r))
            else -> droughtPeriod(weathers, i + 1,
                    type,
                    r.copy(to = weathers[i].second))
        }
    }

    private fun rainPeriod(weathers: List<Pair<Weather, LocalDate>>, i: Int,
                           type: WeatherType = WeatherType.RAIN,
                           r: RainPeriod = RainPeriod(
                                   from = weathers[i].second,
                                   to = weathers[i].second,
                                   rainestDay = weathers[i].second,
                                   rainestDayIndex = i,
                                   rainestPerimeter = weathers[i].first.perimeter!!)): Pair<Int, List<RainPeriod>> {
        return when {
            i == weathers.size || weathers[i].first.weatherType != WeatherType.RAIN -> Pair(i, listOf(r))
            else -> {
                val (nRainestDay, nRainestPerimeter, nRainestDayIndex) =
                        if (r.rainestPerimeter < weathers[i].first.perimeter!!)
                            Triple(weathers[i].second, weathers[i].first.perimeter, i)
                        else
                            Triple(weathers[r.rainestDayIndex].second, r.rainestPerimeter, r.rainestDayIndex)
                rainPeriod(weathers, i + 1, type,
                        r.copy(
                                to = weathers[i].second,
                                rainestDayIndex = nRainestDayIndex,
                                rainestDay = nRainestDay,
                                rainestPerimeter = nRainestPerimeter!!))
            }
        }
    }
}