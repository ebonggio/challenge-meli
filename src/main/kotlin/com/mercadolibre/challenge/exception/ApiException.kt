package com.mercadolibre.challenge.exception

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException


data class BadRequest(override val message: String, val ex: RuntimeException? = null) :
        ResponseStatusException(HttpStatus.BAD_REQUEST, message, ex)
data class NotFound(override val message: String, val ex: RuntimeException? = null) :
        ResponseStatusException(HttpStatus.NOT_FOUND, message, ex)
data class InternalServerError(override val message: String, val ex: RuntimeException? = null) :
        ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message, ex)