package com.mercadolibre.challenge

import com.mercadolibre.challenge.listener.KafkaPlanetListener
import com.mercadolibre.challenge.listener.PostgresPlanetListener
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableCassandraRepositories(basePackages = ["com.mercadolibre.challenge.repository.cassandra"])
@EnableJpaRepositories(basePackages = ["com.mercadolibre.challenge.repository.postgres"])
@EnableConfigurationProperties(value = [PostgresPlanetListener.DataSourceProperties::class, KafkaPlanetListener.DataSourceProperties::class])
class ChallengeApplication

fun main(args: Array<String>) {
    runApplication<ChallengeApplication>(*args)
}
